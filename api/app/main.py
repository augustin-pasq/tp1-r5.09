from bson import ObjectId
from fastapi import FastAPI
from pydantic import BaseModel
from pymongo import MongoClient

class Product(BaseModel):
    description: str
    quantity: int

app = FastAPI()
client = MongoClient("mongodb://172.18.0.2:27017/")
productsCollection = client["TP1Database"]["Products"]

@app.post("/addProduct")
async def addProduct(product: Product):
    product_id = productsCollection.insert_one(product.model_dump()).inserted_id
    
    product = productsCollection.find_one({"_id": product_id})
    product["_id"] = str(product["_id"])
    
    return product

@app.put("/editProduct/{id}")
async def editProduct(id: str, product: Product):
    current_product = productsCollection.find_one({"_id": ObjectId(id)})
    product = product.model_dump()

    current_product["description"] = product["description"] if product["description"] else current_product["description"]
    current_product["quantity"] = product["quantity"] if product["quantity"] else current_product["quantity"]

    productsCollection.update_one({"_id": ObjectId(id)}, {"$set": current_product})

    current_product["_id"] = str(current_product["_id"])

    return current_product

@app.delete("/deleteProduct/{id}")
async def deleteProduct(id: str):
    product = productsCollection.find_one({"_id": ObjectId(id)})
    product["_id"] = str(product["_id"])

    productsCollection.delete_one({"_id": ObjectId(id)})

    return product

@app.get("/getProduct/{id}")
async def getProduct(id: str):
    product = productsCollection.find_one({"_id": ObjectId(id)})
    product["_id"] = str(product["_id"])
    
    return product

@app.get("/getProducts")
async def getProduct():
    products = productsCollection.find()
    
    result = []
    for product in products:
        product["_id"] = str(product["_id"])
        result.append(product)

    return result